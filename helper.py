# This is for a dummy tokenizer and preprocessor to be used in TfidfVectorizer
# since we already tokenized and preprocessed our data.
def identity(x):
    return x