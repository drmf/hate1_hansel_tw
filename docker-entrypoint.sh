#!/bin/sh

exec venv/bin/gunicorn --bind=0.0.0.0:8000 --timeout 120 "--workers=$WORKERS" --worker-tmp-dir=/dev/shm "$@" hate1_hansel_flasked:app
