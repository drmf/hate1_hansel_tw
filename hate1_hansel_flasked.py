import pickle

from flask import Flask, request
from flask_json import FlaskJSON, JsonError, as_json
import pickle
from helper import identity

app = Flask(__name__)
# Don't add an extra "status" property to JSON responses - this would break the API contract
app.config['JSON_ADD_STATUS'] = False
# Don't sort properties alphabetically in response JSON
app.config['JSON_SORT_KEYS'] = False

json_app = FlaskJSON(app)


@json_app.invalid_json_error
def invalid_request_error(e):
    """Generates a valid ELG "failure" response if the request cannot be parsed"""
    raise JsonError(status_=400, failure={'errors': [
        {'code': 'elg.request.invalid', 'text': 'Invalid request message'}
    ]})


@app.route('/process', methods=['POST'])
@as_json
def process_request():
    """Main request processing logic - accepts a JSON request and returns a JSON response."""
    data = request.get_json()
    # sanity checks on the request message
    if data.get('type') == 'text':
        # single item
        if 'content' not in data:
            invalid_request_error(None)

        content = data['content']
        result = do_nlp(content)
        return dict(response={'type': 'classification', 'classes': result})
    elif data.get('type') == 'structuredText':
        if ('texts' not in data) or any('content' not in t for t in data['texts']):
            invalid_request_error(None)

        content = [t['content'] for t in data['texts']]
        result = do_nlp(content)
        # result is a list of {'class':'xxx'}, parallel to the input 'texts' list, so
        # represent the output as "annotations" where the classification for text N
        # (zero-based index) is an annotation from N to N+1
        return dict(response={
            'type': 'annotations',
            'annotations':{
                'classes':[{'start': i, 'end':i+1, 'features': val} for (i, val) in enumerate(result)]
            }
        })
    else:
        invalid_request_error(None)


@app.before_first_request
def load_models():
    # load classifier
    global model
    with open('classifier.pickle', 'rb') as handle:
        model = pickle.load(handle)
    global vectorizer
    with open('vectorizer.pickle', 'rb') as handle:
        vectorizer = pickle.load(handle)


def label_name(x):
    if int(x) == 0:
        return 'not hate speech'
    return 'hate speech'


def do_nlp(content):
    # n samples, one label
    if type(content) is str:
        content = [content]
    classes = []
    test_x = vectorizer.transform(content)
    test_y = model.predict(test_x)
    results = []
    for label in test_y:
        results.append({'class': label_name(label)})
    return results


if __name__ == '__main__':
    app.run()
